package main

import (
	"crypto/sha256"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"gitlab.com/inyutin/pbc-go"
)

var clients = make(map[*websocket.Conn]bool) // connected clients
var broadcast = make(chan Message)           // broadcast channel
var receive = make(chan Message)             // broadcast channel

// Configure the upgrader
var upgrader = websocket.Upgrader{}

var sharedParams string
var sharedG []byte

// Define our message object
type Message struct {
	Email     string `json:"email"`
	Username  string `json:"username"`
	Message   string `json:"message"`
	signature []byte
	pubKey    []byte
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}
	// Make sure we close the connection when the function returns
	defer ws.Close()

	// Register our new client
	clients[ws] = true

	pairing, _ := pbcGo.NewPairingFromString(sharedParams)
	g := pairing.NewG2().SetBytes(sharedG)

	// Generate keypair (x, g^x)
	privKey := pairing.NewZr().Rand()
	pubKey := pairing.NewG2().PowZn(g, privKey)

	for {
		var msg Message
		// Read in a new message as JSON and map it to a Message object
		err := ws.ReadJSON(&msg)
		msg.pubKey = pubKey.Bytes()

		h := pairing.NewG1().SetFromStringHash(msg.Message, sha256.New())
		signature := pairing.NewG2().PowZn(h, privKey)

		msg.signature = signature.Bytes()

		if err != nil {
			log.Printf("error: %v", err)
			delete(clients, ws)
			break
		}
		// Send the newly received message to the broadcast channel
		broadcast <- msg
	}
}

func handleMessages() {
	pairing, _ := pbcGo.NewPairingFromString(sharedParams)
	g := pairing.NewG2().SetBytes(sharedG)

	for {
		// Grab the next message from the broadcast channel
		msg := <-broadcast
		pubKey := pairing.NewG2().SetBytes(msg.pubKey)
		signature := pairing.NewG1().SetBytes(msg.signature)

		h := pairing.NewG1().SetFromStringHash(msg.Message, sha256.New())
		temp1 := pairing.NewGT().Pair(h, pubKey)
		temp2 := pairing.NewGT().Pair(signature, g)

		// Send it out to every client that is currently connected
		if !temp1.Equals(temp2) {
			fmt.Println("*BUG* Signature check failed *BUG*")
		} else {
			fmt.Println("Signature to server verified correctly")
			for client := range clients {
				err := client.WriteJSON(msg)
				if err != nil {
					log.Printf("error: %v", err)
					client.Close()
					delete(clients, client)
				}
			}
		}
	}
}

func main() {
	// Generate params for transactions
	params := pbcGo.GenerateA(160, 512)
	pairing := params.NewPairing()
	g := pairing.NewG2().Rand()

	sharedParams = params.String()
	sharedG = g.Bytes()

	// Create a simple file server
	fs := http.FileServer(http.Dir("."))
	http.Handle("/", fs)

	// Configure websocket route
	http.HandleFunc("/ws", handleConnections)

	// Start listening for incoming chat messages
	go handleMessages()
	// Start the server on localhost port 8000 and log any errors
	log.Println("http server started on :7000")
	err := http.ListenAndServe(":7000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
