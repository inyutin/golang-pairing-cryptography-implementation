This is repository for the task "Golang pairing cryptography implementation" by Fantom Foundation

## Structure
This repository consists only example of usage.  
I also provide a library, which you can find [here](https://gitlab.com/inyutin/pbc-go)  

## Dependencies
    PBC - https://github.com/blynn/pbc
    GMP - https://github.com/lattera/glibc

## Usage 
After installing all dependencies you can run `go run main.go` or just `./main`
It will generate pairing key and sign transaction between Alice and Bob.  
We clarify that this is just an example and it can be scaled to transfer data from different machines

![](img/example.png)

## Docker
To run this app through docker:  
``` docker build --no-cache -t pbc_go . ```  
``` docker run pbc_go ```

Docker runs example of usage

--------------------------------------------------------------------------------

# Chat

I also implemented a web chat to demonstrate the work of my solution.

![](img/chat.png)

Usage:

`docker build --no-cache -t chat_go ./src/public/`  
`docker run --network="host" chat_go:latest`  

To see how the app works with multiple users, just open another browser tab or window and navigate to "http://localhost:7000".  
Enter a different email and username. Take turns sending messages from both windows.




