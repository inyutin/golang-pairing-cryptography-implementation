FROM ubuntu

    
RUN apt-get update -y
RUN apt-get install -y git gcc wget build-essential flex bison libgmp3-dev

RUN wget https://crypto.stanford.edu/pbc/files/pbc-0.5.14.tar.gz && \
    tar -xvf pbc-0.5.14.tar.gz && \
    cd pbc-0.5.14 && \
    ./configure && \
    make &&  \ 
    make install

RUN ldconfig -v

RUN rm pbc-0.5.14.tar.gz && rm -rf pbc-0.5.14 

ADD main /

CMD ["/main"]

